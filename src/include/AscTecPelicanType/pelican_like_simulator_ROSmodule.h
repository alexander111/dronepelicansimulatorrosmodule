#ifndef PELICAN_LIKE_SIMULATOR_ROSMODULE_H
#define PELICAN_LIKE_SIMULATOR_ROSMODULE_H

#include <iostream>
#include <cmath>

#include "ros/ros.h"
#include "droneModuleROS.h"
#include "communication_definition.h"

#include "AscTecPelicanType/pelican_like_simulator.h"

// ROS messages
#include "px_comm/OpticalFlow.h"
#include "asctec_msgs/LLStatus.h"
#include "asctec_msgs/IMUCalcData.h"
#include "asctec_msgs/CtrlInput.h"
#include "droneMsgsROS/dronePoseStamped.h"

#include "Timer.h"


///Consts
const double DRONE_PELICAN_LIKE_SIMULATOR_RATE = 200.0;
const double DRONE_PELICAN_LIKE_SIMULATOR_BATTERY_TIME = 120.0;
//#define DRONE_PELICAN_LIKE_SIMULATOR_ACTIVATE_BATTERY_TIME

class PelicanLikeSimulatorROSModule : public DroneModule
{
public:
    //Constructor and destructor
    PelicanLikeSimulatorROSModule( const std::string &drone_config_filename=std::string("pelican_like_model.xml") );
    ~PelicanLikeSimulatorROSModule();
    // ROSmodule generic functions
public:
    void init();
    void close();
    void open(ros::NodeHandle & nIn, std::string moduleName);
    bool run();
protected:
    bool resetValues();
    bool startVal();
    bool stopVal();

protected:
    // Dynamics simulator
    PelicanLikeSimulator MyDroneSimulator;
    // Publishers and subscribers
    ros::Publisher  ground_speed_sensor_publisher;  // msg_type: px_comm/OpticalFlow
    bool publishGroundSpeedSensorData();
    ros::Publisher  low_level_status_publisher;     // msg_type: asctec_msgs/LLStatus
    bool publishLowLevelStatusData();
    ros::Publisher  imu_calcdata_publisher;         // msg_type: asctec_msgs/IMUCalcData
    bool publishIMUCalcdataData();
    ros::Publisher dronePosePubl;                   // msg_type: droneMsgsROS/dronePose, topic: DRONE_SIMULATOR_INTERNAL_POSE
    int  publishDronePose();
    ros::Subscriber control_input_subscriber;       // msg_type: asctec_msgs/CtrlInput
    void controlInputCallback(const asctec_msgs::CtrlInput::ConstPtr& msg);

    Timer battery_timer;
};

#endif // PELICAN_LIKE_SIMULATOR_ROSMODULE_H
