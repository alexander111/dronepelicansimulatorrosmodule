#include "AscTecPelicanType/pelican_like_simulator_ROSmodule.h"

PelicanLikeSimulatorROSModule::PelicanLikeSimulatorROSModule(const std::string &drone_config_filename) :
    DroneModule(droneModule::active, DRONE_PELICAN_LIKE_SIMULATOR_RATE) ,
    MyDroneSimulator(idDrone,stackPath,drone_config_filename)
{
    init();
    battery_timer.restart(false);
}

PelicanLikeSimulatorROSModule::~PelicanLikeSimulatorROSModule()
{
}

void PelicanLikeSimulatorROSModule::init()
{
}

void PelicanLikeSimulatorROSModule::close()
{
}

void PelicanLikeSimulatorROSModule::open(ros::NodeHandle &nIn, std::string moduleName)
{
    //Node
    DroneModule::open(nIn,moduleName);

    //Subscribers
    control_input_subscriber      = n.subscribe(DRONE_PELICAN_LIKE_SIMULATOR_CONTROL_INPUT_SUBSCRIBER, 1, &PelicanLikeSimulatorROSModule::controlInputCallback, this);

    //Publishers
    ground_speed_sensor_publisher = n.advertise<px_comm::OpticalFlow>(DRONE_PELICAN_LIKE_SIMULATOR_GROUND_SPEED_SENSOR_PUBLISHER, 1, true);
    low_level_status_publisher    = n.advertise<asctec_msgs::LLStatus>(DRONE_PELICAN_LIKE_SIMULATOR_LOW_LEVEL_STATUS_PUBLISHER  , 1, true);
    imu_calcdata_publisher        = n.advertise<asctec_msgs::IMUCalcData>(DRONE_PELICAN_LIKE_SIMULATOR_IMU_CALCDATA_PUBLISHER   , 1, true);
    dronePosePubl        = n.advertise<droneMsgsROS::dronePoseStamped>(DRONE_PELICAN_LIKE_SIMULATOR_DRONEPOSE_PUBLISHER, 1, true);

    //Flag of module opened
    droneModuleOpened=true;

    //Auto-Start module
    moduleStarted=true;

    //End
    return;
}

bool PelicanLikeSimulatorROSModule::resetValues()
{
    return true;
}

bool PelicanLikeSimulatorROSModule::startVal()
{
    return true;
}

bool PelicanLikeSimulatorROSModule::stopVal()
{
    return true;
}

bool PelicanLikeSimulatorROSModule::publishGroundSpeedSensorData()
{
    if(droneModuleOpened==false)
        return false;

    //Fill the msg
    px_comm::OpticalFlow optical_flow_msg;
    optical_flow_msg.header.stamp=ros::Time::now();
    optical_flow_msg.quality = 1;
    optical_flow_msg.flow_x  = 0;
    optical_flow_msg.flow_y  = 0;
    {
    double altitudeOut, varAltitudeOut;
    MyDroneSimulator.altitudeSensor.getAltitude( altitudeOut, varAltitudeOut);
    double vxOut, vyOut;
    MyDroneSimulator.groundSpeedSensor.getGroundSpeed( vxOut, vyOut);

    // m,   altitude
    optical_flow_msg.ground_distance = (float) -altitudeOut;
    // m/s,  mavwork reference frame
    optical_flow_msg.velocity_x      = (float)  vxOut;
    optical_flow_msg.velocity_y      = (float)  vyOut;
    }

    //Publish
    ground_speed_sensor_publisher.publish(optical_flow_msg);
    return true;
}

bool PelicanLikeSimulatorROSModule::publishLowLevelStatusData()
{
    if(droneModuleOpened==false)
        return false;

    //Fill the msg
    asctec_msgs::LLStatus low_level_status_msg;
    low_level_status_msg.header.stamp      = ros::Time::now();
#ifdef DRONE_PELICAN_LIKE_SIMULATOR_ACTIVATE_BATTERY_TIME
    double current_battery_time = battery_timer.getElapsedSeconds();
    double current_simulated_battery_level = 9900 + (12600 - 9900) * (1.0 - current_battery_time/DRONE_PELICAN_LIKE_SIMULATOR_BATTERY_TIME);
    if (current_simulated_battery_level<0) current_simulated_battery_level = 0;
    low_level_status_msg.battery_voltage_1 = current_simulated_battery_level; // mV
    low_level_status_msg.battery_voltage_2 = current_simulated_battery_level; // mV
#else
    low_level_status_msg.battery_voltage_1 = 12000; // mV
    low_level_status_msg.battery_voltage_2 = 12000; // mV
#endif // DRONE_PELICAN_LIKE_SIMULATOR_ACTIVATE_BATTERY_TIME

    low_level_status_msg.status          = 1;
    low_level_status_msg.cpu_load        = 1000;
    low_level_status_msg.compass_enabled = 1;
    low_level_status_msg.chksum_error    = 0;
    low_level_status_msg.flying          = 1;
    low_level_status_msg.motors_on       = 1;
    low_level_status_msg.flightMode      = 1;
    low_level_status_msg.up_time         = 1;

    //Publish
    low_level_status_publisher.publish(low_level_status_msg);
    return true;
}

bool PelicanLikeSimulatorROSModule::publishIMUCalcdataData()
{
    if(droneModuleOpened==false)
        return false;

    //Fill the msg
    asctec_msgs::IMUCalcData imu_calcdata_status_msg;
    imu_calcdata_status_msg.header.stamp = ros::Time::now();

    {
        double yaw_deg, pitch_deg, roll_deg;
        MyDroneSimulator.rotationAnglesSensor.getRotationAngles( yaw_deg, pitch_deg, roll_deg);

        // mdeg, mavwork reference frame
        imu_calcdata_status_msg.angle_nick = (1000.0)*pitch_deg;
        imu_calcdata_status_msg.angle_roll = (1000.0)*roll_deg;
        imu_calcdata_status_msg.angle_yaw  = (1000.0)*yaw_deg;
    }

    //Publish
    imu_calcdata_publisher.publish(imu_calcdata_status_msg);
    return true;
}

int PelicanLikeSimulatorROSModule::publishDronePose()
{
    if(droneModuleOpened==false)
        return false;

    droneMsgsROS::dronePoseStamped estimated_pose_msg;

    double x, y, z, yaw, pitch, roll;
    MyDroneSimulator.getPosition_drone_GMR_wrt_GFF(x, y, z, yaw, pitch, roll);

    estimated_pose_msg.pose.pitch = static_cast<float>(pitch);
    estimated_pose_msg.pose.roll  = static_cast<float>(roll);
    estimated_pose_msg.pose.yaw   = static_cast<float>(yaw);
    estimated_pose_msg.pose.x     = static_cast<float>(x);
    estimated_pose_msg.pose.y     = static_cast<float>(y);
    estimated_pose_msg.pose.z     = static_cast<float>(z);

    estimated_pose_msg.header.stamp=ros::Time::now();
    estimated_pose_msg.pose.reference_frame = "GFF";
    estimated_pose_msg.pose.YPR_system      = "drone_GMR";
    estimated_pose_msg.pose.target_frame    = "wYvPuR";

    dronePosePubl.publish(estimated_pose_msg);
    return 1;
}

bool PelicanLikeSimulatorROSModule::run()
{
    if(!DroneModule::run())
    {
        return false;
    }

    //Evaluate DroneSimulator
    MyDroneSimulator.run();

    //Publish everything
    publishGroundSpeedSensorData();
    publishIMUCalcdataData();
    publishLowLevelStatusData();
    publishDronePose();

    //End
    return true;
}

void PelicanLikeSimulatorROSModule::controlInputCallback(const asctec_msgs::CtrlInput::ConstPtr &msg)
{
    double pitch_cmd  = 0;
    double roll_cmd   = 0;
    double dyaw_cmd   = 0;
    double thrust_cmd = 0;

    if ( (bool)(msg->ctrl & 0b1000) ) // thrust
        thrust_cmd = static_cast<double>(msg->thrust);
    if ( (bool)(msg->ctrl & 0b0100) ) // yaw
        dyaw_cmd   = static_cast<double>(msg->yaw);
    if ( (bool)(msg->ctrl & 0b0010) ) // roll
        roll_cmd   = static_cast<double>(msg->roll);
    if ( (bool)(msg->ctrl & 0b0001) ) // pitch
        pitch_cmd  = static_cast<double>(msg->pitch);

    // saturate pitch
    if (pitch_cmd < -2047) {
        pitch_cmd = -2047;
    } else {
        if (2047 < pitch_cmd) {
            pitch_cmd = 2047;
        }
    }

    // saturate roll
    if (roll_cmd < -2047) {
        roll_cmd = -2047;
    } else {
        if (2047 < roll_cmd) {
            roll_cmd = 2047;
        }
    }

    // saturate dyaw
    if (dyaw_cmd < -2047) {
        dyaw_cmd = -2047;
    } else {
        if (2047 < dyaw_cmd) {
            dyaw_cmd = 2047;
        }
    }

    // saturate thrust
    if (thrust_cmd < 0) {
        thrust_cmd = 0;
    } else {
        if (4095 < thrust_cmd) {
            thrust_cmd = 4095;
        }
    }

    MyDroneSimulator.setInputs( pitch_cmd, roll_cmd, dyaw_cmd, thrust_cmd);


}
