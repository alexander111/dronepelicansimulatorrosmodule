#include "joy2cvgstack/joyconverterpelicancmd.h"


JoyConverterPelicanCmd::JoyConverterPelicanCmd() : DroneModule(droneModule::active)
{
    init();
    return;
}

JoyConverterPelicanCmd::~JoyConverterPelicanCmd()
{

    return;
}

void JoyConverterPelicanCmd::init()
{
    return;
}

void JoyConverterPelicanCmd::close()
{
    return;
}

void JoyConverterPelicanCmd::open(ros::NodeHandle & nIn, std::string moduleName)
{
    //Node
    DroneModule::open(nIn,moduleName);

    //init();


    //Configuration


    //Publisher
    OutputPubl = n.advertise<asctec_msgs::CtrlInput>(DRONE_PELICAN_LIKE_SIMULATOR_CONTROL_INPUT_SUBSCRIBER, 1, true);


    //Subscriber
    InputSubs=n.subscribe("joy", 1, &JoyConverterPelicanCmd::inputCallback, this);


    //Flag of module opened
    droneModuleOpened=true;

    //Auto-Start module
    moduleStarted=true;

    //End
    return;
}

//Reset
bool JoyConverterPelicanCmd::resetValues()
{
    return true;
}

//Start
bool JoyConverterPelicanCmd::startVal()
{
    return true;
}

//Stop
bool JoyConverterPelicanCmd::stopVal()
{
    return true;
}

//Run
bool JoyConverterPelicanCmd::run()
{
    if(!DroneModule::run())
    {
        return false;
    }

    return true;
}

#define KEEP_IN_RANGE(a, min, max) if (a < min) a = min; else if (a > max) a = max;

void JoyConverterPelicanCmd::inputCallback(const sensor_msgs::Joy::ConstPtr& msg)
{
    //Asynchronous module with only one callback!
    if(!run())
        return;

//  # Received input from joypad "Thrustmaster, Dual Analog 3", note that this
//  #   joypad is very low quality, don't fly the Pelican with it!!!!
//  #     msg->axes[0] // stick: left -horizontal, left:+1 right:-1 //   dyaw
//  #     msg->axes[1] // stick: left -vertical  ,   up:+1  down:-1 // thrust
//  #     msg->axes[2] // stick: right-horizontal, left:+1 right:-1 //   roll
//  #     msg->axes[3] // stick: right-vertical  ,   up:+1  down:-1 //  pitch
//  std::cout << " pitch:" << msg->axes[3] << std::endl;
//  std::cout << "  roll:" << msg->axes[2] << std::endl;
//  std::cout << "  dyaw:" << msg->axes[0] << std::endl;
//  std::cout << "thrust:" << msg->axes[1] << std::endl;

//  # Desired joypad-vs-pelican behaviour
//  #   signs in the code are assigned to obtain the desired behaviour.
//  #   The yaw sign test was not done during flight, so it might be wrong.
//  #     stick: left -horizontal, left:      dyaw-leftwards   right:        dyaw-rightwards
//  #     stick: left -vertical  ,   up:     thrust-upwards     down:     thrust-downwards
//  #     stick: right-horizontal, left: horizontal-left       right: horizontal-left
//  #     stick: right-vertical  ,   up: horizontal-frontwards  down: horizontal-backwards

    asctec_msgs::CtrlInput OutputMsgs;
    OutputMsgs.pitch  = (int16_t) (-1.0)*(  msg->axes[3]        * 2047.0 );
    OutputMsgs.roll   = (int16_t) (-1.0)*(  msg->axes[2]        * 2047.0 );
    OutputMsgs.yaw    = (int16_t) (-1.0)*(  msg->axes[0]        * 2047.0 );
    OutputMsgs.thrust = (int16_t) (+1.0)*( (msg->axes[1] + 1.0) * 2047.0 );

    KEEP_IN_RANGE( OutputMsgs.pitch,  -2047, +2047)
    KEEP_IN_RANGE( OutputMsgs.roll,   -2047, +2047)
    KEEP_IN_RANGE( OutputMsgs.yaw,    -2047, +2047)
    KEEP_IN_RANGE( OutputMsgs.thrust,     0, +4095)

//  # CTRL_INPUT.ctrl: bit masks that switch/enable the separate control commands:
//  #   thrust: 0b1000
//  #   yaw   : 0b0100
//  #   roll  : 0b0010
//  #   pitch : 0b0001
//  # example code: if (enable_ctrl_thrust_) ctrl_input_msg->ctrl |= 0b1000
    OutputMsgs.ctrl   = int(0b1111);

//    // Motors power on test:
//    if (OutputMsgs.thrust > 1000) {
//        OutputMsgs.yaw    = (int16_t) (   0);
//        OutputMsgs.thrust = (int16_t) (   0);
//    } else {
//        OutputMsgs.yaw    = (int16_t) (1753);
//        OutputMsgs.thrust = (int16_t) ( 199);
//    }
//    // Tests seem to indicate that the start/stop Pelican motors conditions are:
//    //   yaw    >= 1753
//    //   thrust <=  199

//  # This is the correct code for the CTRL_INPUT message chksum
    OutputMsgs.chksum = (short)0xAAAA + OutputMsgs.thrust + OutputMsgs.ctrl + OutputMsgs.pitch + OutputMsgs.roll + OutputMsgs.yaw;

    //Publish
    OutputPubl.publish(OutputMsgs);
    return;
}
