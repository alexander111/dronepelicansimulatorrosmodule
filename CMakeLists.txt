cmake_minimum_required(VERSION 2.8.3)

set(PROJECT_NAME dronePelicanSimulatorROSModule)
project(${PROJECT_NAME})

### Use version C++03 of the C++ standard
add_definitions(-std=c++03)

# Set the build type.  Options are:
#  Coverage       : w/ debug symbols, w/o optimization, w/ code-coverage
#  Debug          : w/ debug symbols, w/o optimization
#  Release        : w/o debug symbols, w/ optimization
#  RelWithDebInfo : w/ debug symbols, w/ optimization
#  MinSizeRel     : w/o debug symbols, w/ optimization, stripped binaries




set(DRONE_PELICAN_SIMULATOR_SOURCE_DIR
        src/source)
	
set(DRONE_PELICAN_SIMULATOR_INCLUDE_DIR
	src/include
	)

set(PELICAN_LIKE_SIMULATOR_SOURCE_FILES
        ${DRONE_PELICAN_SIMULATOR_SOURCE_DIR}/AscTecPelicanType/pelican_like_simulator_ROSmodule.cpp
        )

set(PELICAN_LIKE_SIMULATOR_HEADER_FILES
        ${DRONE_PELICAN_SIMULATOR_INCLUDE_DIR}/AscTecPelicanType/pelican_like_simulator_ROSmodule.h
        )

set(JOY_CONVERTER_SOURCE_FILES
        ${DRONE_PELICAN_SIMULATOR_SOURCE_DIR}/joy2cvgstack/joyconverterpelicancmd.cpp
        
        )

set(JOY_CONVERTER_HEADER_FILES
        ${DRONE_PELICAN_SIMULATOR_INCLUDE_DIR}/joy2cvgstack/joyconverterpelicancmd.h
        )


find_package(catkin REQUIRED
                COMPONENTS roscpp std_msgs geometry_msgs sensor_msgs droneModuleROS droneMsgsROS droneSimulator lib_cvgutils asctec_msgs px_comm)


catkin_package(
        CATKIN_DEPENDS roscpp std_msgs geometry_msgs sensor_msgs droneModuleROS droneMsgsROS droneSimulator lib_cvgutils asctec_msgs px_comm
  )


include_directories(${DRONE_PELICAN_SIMULATOR_INCLUDE_DIR})
include_directories(${catkin_INCLUDE_DIRS})



add_library(dronePelicanSimulatorROSModule ${PELICAN_LIKE_SIMULATOR_SOURCE_FILES} ${PELICAN_LIKE_SIMULATOR_HEADER_FILES})
add_dependencies(dronePelicanSimulatorROSModule ${catkin_EXPORTED_TARGETS})
target_link_libraries(dronePelicanSimulatorROSModule ${catkin_LIBRARIES})


add_executable(pelicanLikeSimulatorROSModuleNode ${DRONE_PELICAN_SIMULATOR_SOURCE_DIR}/AscTecPelicanType/pelican_like_simulator_ROSmoduleNode.cpp)
add_dependencies(pelicanLikeSimulatorROSModuleNode ${catkin_EXPORTED_TARGETS})
target_link_libraries(pelicanLikeSimulatorROSModuleNode dronePelicanSimulatorROSModule)
target_link_libraries(pelicanLikeSimulatorROSModuleNode ${catkin_LIBRARIES})



add_library(joy_converter_to_pelican_cmd_lib ${JOY_CONVERTER_SOURCE_FILES} ${JOY_CONVERTER_HEADER_FILES})
add_dependencies(joy_converter_to_pelican_cmd_lib ${catkin_EXPORTED_TARGETS})
target_link_libraries(joy_converter_to_pelican_cmd_lib ${catkin_LIBRARIES})


add_executable(joy_converter_to_pelican_cmd ${DRONE_PELICAN_SIMULATOR_SOURCE_DIR}/joy2cvgstack/joyconverterpelicancmdNode.cpp)
add_dependencies(joy_converter_to_pelican_cmd ${catkin_EXPORTED_TARGETS})
target_link_libraries(joy_converter_to_pelican_cmd joy_converter_to_pelican_cmd_lib)
target_link_libraries(joy_converter_to_pelican_cmd ${catkin_LIBRARIES})


